//imports
const express = require("express")
const router = express.Router()
const auth = require('../auth')
const userController = require('../controllers/userControllers')

/*
	verify is a method from our auth module which will let us verify the jwt is legitimate and let us decode the payload.
*/

const {verify} = auth
const {
		registerUser, loginUser, enroll, checkEmailExists,
		singleUser, userEnrollments,
		updateProfile
	  } = userController

//POST
router.post('/', registerUser)//add user
router.post('/login', loginUser)//login user w/ authentication
router.post('/enroll', verify, enroll)//enroll a logged in user
router.post('/checkedEmailExists', checkEmailExists)// check if user's email exists 

//GET
router.get('/getUserDetails', verify, singleUser)//get single user by id
router.get('/getEnrollments', verify, userEnrollments)// get enrollments of logged users

//PUT
router.put('/updateProfile', verify, updateProfile)// update firstName , lastName and mobileNo of user.

/*
	Route methods, much like middleware, give access to req, res and next object for the functions that are included in them.
	In fact, in Expressjs, we can  multiple layers of middleware to do tasks before letting another function perform another tasks. 

*/

//exports
module.exports = router