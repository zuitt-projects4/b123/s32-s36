//imports
const express = require("express")
const router = express.Router()
const auth = require('../auth')
const courseController = require('../controllers/courseControllers')

//deconstruction of controller and auth
const {
	addCourse, 
	getAllCourse, getAllActiveCourse, getSingleCourse, getEnrollees,
	updateCourse, archiveCourse, activateCourse
	} = courseController

const {verify, verifyAdmin} = auth

//routes
//POST
router.post('/',verify, verifyAdmin, addCourse)//add course with admin 

//GET
router.get('/', verify, verifyAdmin, getAllCourse) // get courses with admin
router.get('/getActiveCourses', getAllActiveCourse) // get all active
router.get('/getSingleCourses/:id', getSingleCourse) // get single course'
router.get('/getEnrollees/:id', verify, verifyAdmin, getEnrollees) // get single course'


//PUT
router.put('/:id', verify, verifyAdmin, updateCourse)// update course by name, desc, price
router.put('/archive/:id', verify, verifyAdmin, archiveCourse)// archive a course by id
router.put('/activate/:id', verify, verifyAdmin, activateCourse)// activate a course by id

//exports
module.exports = router