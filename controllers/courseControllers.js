//imports

const Course = require('../models/Courses')
const User = require('../models/User')

//POST METHODS
//add course to db
module.exports.addCourse = (req , res) => {

	console.log(req.body)
	Course.findOne({name: req.body.name})
	.then(result =>{

		console.log(result)
		if(result !== null && result.name === req.body.name){
			
			return res.send("Duplicate Task Found")

		}else{

			let newCourse = new Course({
				name: req.body.name,
				description: req.body.description,
				price: req.body.price,
				/*isActive: req.body.isActive,
				createdOn: req.body.createdOn,
				enrollees: [
					{
						userId: req.body.userId,
						enrolledOn: req.body.enrolledOn, 
						status: req.body.status
					}
				]*/
			})

			newCourse.save()
			.then(result => res.send(result))
			.catch(err => res.send(err))
		}

	})
	.catch(err => res.send(err))

}


//GET METHODS
//get all courses with Admin authentication
module.exports.getAllCourse = (req, res) =>{

	Course.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err))

}


//get all active courses
module.exports.getAllActiveCourse = (req, res) => {

	Course.find({isActive : true})
	.then(result => res.send(result))
	.catch(err => res.send(err))

}

//get single course by id
module.exports.getSingleCourse = (req, res) =>{

	Course.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))

}

//get enrollees on the course
module.exports.getEnrollees = (req, res) => {

	Course.findById(req.params.id)
	.then(course => res.send(course.enrollees))
	.catch(err => res.send(err))

}

//PUT METHODS
//update course document by name, desc, price
module.exports.updateCourse = (req, res) => {

	console.log(req.params)


	let updates = {
		
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err))


}

//archive course by id
module.exports.archiveCourse = (req, res) => {

	console.log(req.params)

	let updates = {isActive: false}

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err))

}

//activate course by id
module.exports.activateCourse = (req, res) => {

	console.log(req.params)

	let updates = {isActive: true}

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err))

}

/*
	res.send() 
		- always return it
		- should always be the last process done.
		- No other steps within the codeblock should be done after it.
*/