 const User = require('../models/User')
 const Course = require('../models/Courses')
 const bcrypt = require('bcrypt')
 const auth = require('../auth')

 const {createAccessToken} = auth
/*
	import the auth module and deconstruct to get our createAccessToken method.

	bcrypt has methods which will help us add a layer of security for our user's passwords.

	What bcrypt does is hash our password string into a randomized character versions of your password.

	It is able to hide your password within that ramdomized string. 

	syntax: const <variable> = bcrypt.hashSync(<stringToBeHashed>, <saltRounds>)

	 <saltRounds> - are the numbers of times the character in the hash are randomized.
	 			  - 10 is the most sufficient number to use


*/
//POST METHODS
//register a new user
module.exports.registerUser = (req , res) => {

	console.log(req.body)

	if(req.body.password.length < 8) return res.send({message: "Password is too short."})

	const hashedPW = bcrypt.hashSync(req.body.password, 10)
	console.log(hashedPW)

	
	let newUser = new User({
		firstName:req.body.firstName,
		lastName:req.body.lastName,
		mobileNo:req.body.mobileNo,
		email:req.body.email,
		password:hashedPW
		
	})

	newUser.save()
	.then(result => res.send(result))
	.catch(err => res.send(err))

}

//login a user
module.exports.loginUser = (req, res) => {

	console.log(req.body)
	User.findOne({email:req.body.email})
	.then(foundRes => {

		if(foundRes === null){
			return res.send({message: "No User Found"})
		}else{
	/* 		
			console.log(req.body.email)
			console.log(req.body.password)

			if we found a user, result will contain the found user's documents
			bcrypt.compareSync(<string>, <hashedString>)
			 - returns a boolean after comparing the 1st argument and the hashed version of that string. if it matches, it will return true and if it doesn't it returns false.

			In our Booking system's login, we compare the input password and the hashed password from the database.

	*/
		const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundRes.password)
		console.log(isPasswordCorrect)

			if (isPasswordCorrect){
				/*console.log("We will generate a key")*/
				return res.send({accessToken: createAccessToken(foundRes)})
			}else{
				return res.send({message: "Password is incorrect"})
			}
		}

	})
	.catch(err => res.send(err))
}

/*
		- JS doesn't wait for your variable/function finish process/ finished first before continuing.
		- It will either stop the whole process or just continue forward.

	async and await
		- with the use of async and await we can wait for the process to finish before we go forward.
		- await can only be used on async function.
		- we add async keyword to make a function asynchronous.

*/
//enroll a user to courses
module.exports.enroll = async (req, res) =>{

	/*
		console.log(req.user.id)
		console.log(req.body.courseId)
	*/

	if(req.user.isAdmin)return res.send({auth: "Failed.", message: "Action Forbidden"})
	

			let isUserUpdated = await User.findById(req.user.id)
			.then(user=>{
				//console.log(user.enrollments)
				user.enrollments.push({courseId: req.body.courseId})
				return user.save()
				.then(user => true)
				.catch(err => err.message)
			})

			console.log(isUserUpdated)
			if(isUserUpdated !== true ) return res.send(isUserUpdated)

			let isCourseUpdated = await Course.findById(req.body.courseId)
			.then(course => {

				course.enrollees.push({userId: req.user.id})

				return course.save()
				.then(user => true)
				.catch(err => err.message)

			})

			console.log(isCourseUpdated)
			if(isCourseUpdated !== true) return res.send(isCourseUpdated)

			if(isUserUpdated && isCourseUpdated) return res.send("Enrolled Successful")


}

//check if user email exists
module.exports.checkEmailExists = (req , res) =>{

	User.findOne({email: req.body.email})
	.then( result => {

		if(result === null){
			return res.send({isAvailable:true, message: "Email Available."})
		}else{
			return res.send({isAvailable: false, message: "Email is already Registered."})
		}
	})
	.catch(err => res.send(err))

}



//GET METHODS
//get a single logged in  user
module.exports.singleUser = (req, res) =>{

	console.log(req.user)

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

//get user Enrollments
module.exports.userEnrollments = (req, res) => {

	User.findById(req.user.id)
	.then(user => res.send(user.enrollments))
	.catch(err => res.send(err))

}


//PUT METHOD
//update user profile by fname, lname, mobilenum
module.exports.updateProfile = (req, res) =>{

	console.log(req.user)

	let updates ={

		firstName : req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo

	}

	User.findByIdAndUpdate(req.user.id, updates, {new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err))

}
