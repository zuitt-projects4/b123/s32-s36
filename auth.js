//imports
const jwt = require('jsonwebtoken')
const secret = "CourseBookingApi"

/*
	JWT is a way to securely pass information from a part of a server to the frontend or other parts of the server.
		- It allows us to create a sort of "keys" which is able to authenticate our users.

	secret passed is any string but to allow access with the token, the secret must be intact.

	JWT is like a  gift wrapping service but with a secret and only our system knows about the secret. 
	 - and if the secret is not intact ot the JWT seems tampered with, we will able to reject the user who used an illegitimate token.
	 - this will ensure that only registered users can do certain things in our app.
	
	data object is created to contain some details of our user.
	only logged in user is handed or given a token 

	jwt.sign(<payload>, <secret>, <algo>) - create your jwt with the payload, with the secret, the algorithm to create your JWT.

*/
//creating token for logged in users
module.exports.createAccessToken = (user) => {

	// console.log(user)

	const data = {

		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {})

}

//verify if it is a user using the token given 
module.exports.verify = (req, res, next) => {

	/*
		token passed as bearer tokens can be found in which part of your request.
		console.log(req.headers.authorization)
		token variable will hold our bearer token from our client
		
		if no token is passed, req.headers.authorization is undefined

		else extract the token and remove the "Bearer" from our token variables

	*/

		let token = req.headers.authorization
		// console.log(token)

		if(typeof token === "undefined"){

			return res.send({auth: "Failed. No Token"})

		}else{

			token = token.slice(7, token.length)
			//console.log(token)
			jwt.verify(token, secret, function(err, decodedToken){
				/*
					err will contain the error from our decoding our token.
					decodedToken is our token after completing and accomplishing verification of its legitimacy against our secret.
				*/
				if(err){
					return res.send({auth: "Failed", message: err.message})
				}else{
					/*
						contains the data payload from our token.
						We will add a new property in the req object called user. Assign the decoded data to that property.
						next() - will allow us to run the next function. another middleware or the controller.
					*/
					console.log(decodedToken)
					req.user = decodedToken
					next()
				}

			})
		}
}

//verificatio for isAdmin
module.exports.verifyAdmin = (req, res, next) =>{

	if(req.user.isAdmin){
		next()
	}else{
		return res.send({auth: "Failed", message: "Action Forbidden"})
	}

}